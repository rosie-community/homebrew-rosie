# To install Rosie Pattern Language on MacOS

First, add this repository to brew.  This is a one-time step.

```shell
brew tap rosie-community/rosie https://gitlab.com/rosie-community/packages/homebrew-rosie.git
```

Now you can install the current version of rosie.  (See below for how to install
the bleeding edge version.)

```shell
brew install rosie
```

# To try the latest bleeding edge release (typically a beta)

```shell
brew install --HEAD rosie
```

Homebrew may ask you to first `brew unlink rosie` to remove the links to the
version already installed.

We *really appreciate* users of beta releases!  Do not hesitate to contact us to
report issues!


# If you get an error

Please report it by opening an issue to this repository, or visit the
[project homepage](https://rosie-lang.org) and using the _Contact us_ link on
the left side menu.


# References

* [Rosie Pattern Language project](https://gitlab.com/rosie-pattern-language/rosie)
* [Homebrew](http://brew.sh) package management for Mac OS X
